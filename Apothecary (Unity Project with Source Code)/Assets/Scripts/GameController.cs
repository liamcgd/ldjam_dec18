﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    [SerializeField] private GameObject patientDoc;
    [SerializeField] private GameObject herbBookClosed;
    private float time = 300;
    private float startTime = 300;
    [SerializeField] private GameObject timeText;
    
    [SerializeField] private GameObject endBook;
    [SerializeField] private GameObject endLeft;
    [SerializeField] private GameObject endRight;
    [SerializeField] private GameObject endBackground;

    private bool timerOn = false;

	// Use this for initialization
	void Start ()
    {
        LoadHerbInformation();
        LoadSymptoms();
        LoadPatients();
        CreatePatientDocs();
        CreateHerbBook();
        StartGame();
	}

    private void LoadHerbInformation()
    {
        const string filepath = "Text Files/Herbs.txt";
        Dictionary<HerbID, Herb> herbs = new Dictionary<HerbID, Herb>();
        StreamReader reader = new StreamReader(filepath);
        int nHerbs = Convert.ToInt32(reader.ReadLine());
        HerbID herbID;
        string herbName;
        string herbDesc;
        int nSymptoms;
        List<SymptomID> curesSymptoms;
        int minHelpful;
        int maxHelpful;
        int minHarmful;
        string negativeEffects;
        int minLethal;

        for (int i = 0; i < nHerbs; i++)
        {
            herbID = (HerbID)Enum.Parse(typeof(HerbID), reader.ReadLine());
            herbName = reader.ReadLine();
            herbDesc = reader.ReadLine();
            nSymptoms = Convert.ToInt32(reader.ReadLine());
            curesSymptoms = new List<SymptomID>();

            for (int j = 0; j < nSymptoms; j++)
            {
                curesSymptoms.Add((SymptomID)Enum.Parse(typeof(SymptomID), reader.ReadLine()));
            }

            minHelpful = Convert.ToInt32(reader.ReadLine());
            maxHelpful = Convert.ToInt32(reader.ReadLine());
            minHarmful = Convert.ToInt32(reader.ReadLine());
            negativeEffects = reader.ReadLine();
            minLethal = Convert.ToInt32(reader.ReadLine());

            herbs.Add(herbID, new Herb(herbID, herbName, herbDesc, curesSymptoms, minHelpful, maxHelpful, minHarmful, negativeEffects, minLethal));
        }

        reader.Close();

        GameObject.Find("Main Camera").GetComponent<Herbs>().ListOfHerbs = herbs;
    }

    private void LoadSymptoms()
    {
        const string filepath = "Text Files/Symptoms.txt";
        Dictionary<SymptomID, Symptom> symptoms = new Dictionary<SymptomID, Symptom>();
        StreamReader reader = new StreamReader(filepath);
        int nSymptoms = Convert.ToInt32(reader.ReadLine());
        SymptomID symptomID;
        string symptomName;
        //string symptomDesc;

        for (int i = 0; i < nSymptoms; i++)
        {
            symptomID = (SymptomID)Enum.Parse(typeof(SymptomID), reader.ReadLine());
            symptomName = reader.ReadLine();
            //symptomDesc = reader.ReadLine();

            symptoms.Add(symptomID, new Symptom(symptomID, symptomName /*, symptomDesc*/));
        }

        reader.Close();

        GameObject.Find("Main Camera").GetComponent<Symptoms>().ListOfSymptoms = symptoms;
    }

    private void LoadPatients()
    {
        const string filepath = "Text Files/Patients.txt";
        Dictionary<SymptomID, Symptom> symptomList = GameObject.Find("Main Camera").GetComponent<Symptoms>().ListOfSymptoms;
        List<Patient> patients = new List<Patient>();
        StreamReader reader = new StreamReader(filepath);
        int nPatients = Convert.ToInt32(reader.ReadLine());
        string name;
        int age;
        string description;
        string symptomsDescription;
        int nSymptoms;
        List<Symptom> symptoms;

        for (int i = 0; i < nPatients; i++)
        {
            name = reader.ReadLine();
            age = Convert.ToInt32(reader.ReadLine());
            description = reader.ReadLine();
            symptomsDescription = reader.ReadLine();
            nSymptoms = Convert.ToInt32(reader.ReadLine());
            symptoms = new List<Symptom>();

            for (int j = 0; j < nSymptoms; j++)
            {
                symptoms.Add(symptomList[(SymptomID)Enum.Parse(typeof(SymptomID), reader.ReadLine())]);
            }

            patients.Add(new Patient(name, age, description, symptomsDescription, symptoms));
        }

        reader.Close();

        GameObject.Find("Main Camera").GetComponent<Patients>().ListOfPatients = patients;
    }

    private void CreatePatientDocs()
    {
        float newX = 0;
        float newZ = -0.55f;

        foreach (Patient p in GameObject.Find("Main Camera").GetComponent<Patients>().ListOfPatients)
        {
            GameObject patDoc = Instantiate(patientDoc);
            patDoc.transform.position += new Vector3(newX, 0, newZ);
            patDoc.GetComponent<PatientDoc>();
            Patient temp = patDoc.GetComponent<PatientDoc>().Patient;
            patDoc.GetComponent<PatientDoc>().Patient = p;
            patDoc.GetComponent<PatientDoc>().WritePatient();
            newX += 1f;
            newZ += 0.1f;
        }
    }

    private void CreateHerbBook()
    {
        Instantiate(herbBookClosed);
    }

    private void StartGame()
    {
        GameObject eb = Instantiate(endBook);
        GameObject lp = Instantiate(endLeft);
        GameObject rp = Instantiate(endRight);
        GameObject ebb = Instantiate(endBackground);
        List<string> startPages = GetStartBookPages();

        eb.GetComponent<EndBookOpen>().InsertPages(lp.GetComponent<EndBookPage>(), rp.GetComponent<EndBookPage>(), ebb.GetComponent<EndBookBackground>(), startPages);
        lp.GetComponent<EndBookPage>().OpenBook = eb.GetComponent<EndBookOpen>();
        rp.GetComponent<EndBookPage>().OpenBook = eb.GetComponent<EndBookOpen>();
        ebb.GetComponent<EndBookBackground>().OpenBook = eb.GetComponent<EndBookOpen>();
    }

    private List<string> GetStartBookPages()
    {
        List<string> pages = new List<string>();

        pages.Add("You have patients. You have herbs. Close this book to start curing them. Do a poor job or kill any, and you're fired.");

        return pages;
    }

    private void Update()
    {
        if (timerOn)
        {
            time -= Time.deltaTime;

            int min = Mathf.FloorToInt(time) / 60;
            int sec = Mathf.FloorToInt(time) - (min * 60);

            timeText.GetComponent<Text>().text = min + ":" + sec.ToString("00");

            if (time <= 0)
            {
                EndGame();
            }
        }
    }

    public void EndGame()
    {
        GameObject eb = Instantiate(endBook);
        GameObject lp = Instantiate(endLeft);
        GameObject rp = Instantiate(endRight);
        GameObject ebb = Instantiate(endBackground);
        List<string> endBookPages = GetEndBookPages();

        eb.GetComponent<EndBookOpen>().InsertPages(lp.GetComponent<EndBookPage>(), rp.GetComponent<EndBookPage>(), ebb.GetComponent<EndBookBackground>(), endBookPages);
        lp.GetComponent<EndBookPage>().OpenBook = eb.GetComponent<EndBookOpen>();
        rp.GetComponent<EndBookPage>().OpenBook = eb.GetComponent<EndBookOpen>();
        ebb.GetComponent<EndBookBackground>().OpenBook = eb.GetComponent<EndBookOpen>();

        timerOn = false;
    }

    private List<string> GetEndBookPages()
    {
        List<string> pages = new List<string>();

        foreach (Patient p in GetComponent<Patients>().ListOfPatients)
        {
            pages.Add(p.TreatPatient());
        }

        return pages;
    }

    public void NextRound()
    {
        PatientDoc[] docs = FindObjectsOfType<PatientDoc>();

        time = startTime;
        timerOn = true;

        foreach (PatientDoc patDoc in docs)
        {
            Destroy(patDoc.gameObject);
        }

        LoadPatients();
        CreatePatientDocs();
    }
}
