﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoubleClickable : MonoBehaviour
{
    float clickTime;
    float timeDifferenceLimit = 1f;

    private void Start()
    {
        clickTime = Time.time - 2f;
    }

    public bool DoubleClick()
    {
        if (Time.time - clickTime > timeDifferenceLimit)
        {
            clickTime = Time.time;
        }
        else
        {
            clickTime -= 2f;
            return true;
        }

        return false;
    }
}
