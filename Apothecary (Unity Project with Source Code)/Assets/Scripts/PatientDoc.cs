﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatientDoc : TextWrapper
{
    public int lineLength = 40;
    public bool open = false;
    [SerializeField] private Sprite closedDoc;
    [SerializeField] private Sprite openDoc;
    [SerializeField] private AudioSource audioSource;
    public Patient Patient { get; set; }
    private Vector3 closPos;
    private Vector3 closScale;
    private Vector3 openScale;

    private void Start()
    {
        closPos = transform.position;
        closScale = transform.localScale;
        openScale = closScale * 1.5f;
    }

    public void WritePatient()
    {
        transform.Find("Heading1").GetComponent<TextMesh>().text = "Patient Details";
        transform.Find("Body1").GetComponent<TextMesh>().text = "Name: " + Patient.Name
            + "\nAge: " + Patient.Age
            + WrapText(lineLength, "\n\nDescription: " + Patient.Description)
            + WrapText(lineLength, "\n\nSymptoms: " + Patient.SymptomsDescription);

        transform.Find("Heading2").GetComponent<TextMesh>().text = "Prescription";
    }

    private void OnMouseDown()
    {
        OpenCloseDoc();
    }

    public void OpenCloseDoc()
    {
        audioSource.Play();

        open = !open;

        if (open)
        {
            PatientDoc[] docs = FindObjectsOfType<PatientDoc>();
            
            foreach (PatientDoc patDoc in docs)
            {
                if (patDoc == this)
                {
                    continue;
                }
                if (patDoc.open)
                {
                    patDoc.OpenCloseDoc();
                }
            }

            transform.Find("Sprite1").gameObject.GetComponent<SpriteRenderer>().flipX = true;
            transform.Find("Page2").gameObject.SetActive(true);
            transform.Find("Heading1").gameObject.SetActive(true);
            transform.Find("Body1").gameObject.SetActive(true);
            transform.Find("Sprite2").gameObject.SetActive(true);
            transform.Find("Heading2").gameObject.SetActive(true);
            transform.Find("Body2").gameObject.SetActive(true);
            UpdateDisplayedPrescription();
            transform.position = new Vector3(-3.72f, .55f, -1);
            transform.localScale = openScale;
        }
        else
        {
            GetComponentInChildren<SpriteRenderer>().sprite = closedDoc;
            transform.Find("Sprite1").gameObject.GetComponent<SpriteRenderer>().flipX = false;
            transform.Find("Page2").gameObject.SetActive(false);
            transform.Find("Heading1").gameObject.SetActive(false);
            transform.Find("Body1").gameObject.SetActive(false);
            transform.Find("Sprite2").gameObject.SetActive(false);
            transform.Find("Heading2").gameObject.SetActive(false);
            transform.Find("Body2").gameObject.SetActive(false);
            transform.position = closPos;
            transform.localScale = closScale;
        }
    }

    public void UpdateDisplayedPrescription()
    {
        string prescription = "";

        if (Patient.PrescribedHerbs.Count > 0)
        {
            foreach (KeyValuePair<HerbID, Herb> p in Patient.PrescribedHerbs)
            {
                prescription += "\t" + p.Value.Quantity + " " + p.Value.Name + "\r\n";
            }
        }
        else
        {
            prescription = "Nothing";
        }

        transform.Find("Body2").GetComponent<TextMesh>().text = prescription;
    }
}
