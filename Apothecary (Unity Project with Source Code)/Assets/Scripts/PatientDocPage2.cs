﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatientDocPage2 : MonoBehaviour
{
    [SerializeField] PatientDoc doc;

    private void OnMouseDown()
    {
        doc.OpenCloseDoc();
    }
}
