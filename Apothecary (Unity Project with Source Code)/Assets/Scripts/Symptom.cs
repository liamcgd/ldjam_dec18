﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Symptom
{
    private SymptomID symptomID;
    private string symptomName;
    //private string symptomDescription;

    public Symptom(SymptomID id, string name /*, string desc*/)
    {
        symptomID = id;
        symptomName = name;
        //symptomDescription = desc;
    }

    public SymptomID SymptomID
    {
        get
        {
            return symptomID;
        }
    }

    public string SymptomName
    {
        get
        {
            return symptomName;
        }
    }

    /*public string SymptomDescription
    {
        get
        {
            return symptomDescription;
        }
    }*/
}
