﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Herbs : MonoBehaviour
{
    [System.Serializable]
    public class HerbIDSpritePair
    {
        [SerializeField] private HerbID herb;
        [SerializeField] private Sprite sprite;

        public HerbID Herb
        {
            get
            {
                return herb;
            }
        }

        public Sprite Sprite
        {
            get
            {
                return sprite;
            }
        }
    }

    private Dictionary<HerbID, Herb> herbs;
    private Dictionary<HerbID, Sprite> herbSprites = new Dictionary<HerbID, Sprite>();
    [SerializeField] private HerbIDSpritePair[] herbSpritesArray;

    private void Start()
    {
        foreach (HerbIDSpritePair p in herbSpritesArray)
        {
            herbSprites.Add(p.Herb, p.Sprite);
        }
    }

    public Dictionary<HerbID, Herb> ListOfHerbs
    {
        get
        {
            return herbs;
        }

        set
        {
            herbs = value;
            //PrintHerbs();
        }
    }

    public Sprite GetHerbSprite(HerbID herb)
    {
        if (herbSprites.ContainsKey(herb))
        {
            return herbSprites[herb];
        }

        return null;
    }
    
    /*public Dictionary<HerbID, Sprite> HerbSprites
    {
        get
        {
            return HerbSprites;
        }
    }*/

    /*private void PrintHerbs()
    {
        Debug.Log("Printing details for " + herbs.Count + " herbs:");

        foreach (KeyValuePair<HerbID, Herb> p in herbs)
        {
            Debug.Log("ID: " + p.Value.HerbID);
            Debug.Log("Name: " + p.Value.Name);
            Debug.Log("Description: " + p.Value.Description);

            Debug.Log("Treats " + p.Value.TreatableSymptoms.Count + " symptoms:");

            foreach(SymptomID id in p.Value.TreatableSymptoms)
            {
                Debug.Log(id);
            }

            Debug.Log("Minimum helpful dosage: " + p.Value.MinHelpful);
            Debug.Log("Maximum helpful dosage: " + p.Value.MaxHelpful);
            Debug.Log("Minimum harmful dosage: " + p.Value.MinHarmful);
            Debug.Log("Negative effects at harmful dosages: " + p.Value.NegativeEffects);
            Debug.Log("Minimum lethal dosage: " + p.Value.LethalDose);
        }

        Debug.Log("Finished printing herb details");
    }*/
}
