﻿
public enum HerbID
{
    Atractylode,
    BoHe,
    Bupleurum,
    DangGui,
    DaSuan,
    Ephedra,
    Ginger,
    GinkgoBilboa,
    HuangChuJu,
    LicoriceRoot,
    LotusSeed,
    Poeny,
    ReishiMushroom,
    TangKuei
}
