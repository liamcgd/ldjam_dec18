﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGameButton : MonoBehaviour
{
    private void OnMouseDown()
    {
        GameObject.Find("Main Camera").GetComponent<GameController>().EndGame();
    }
}
