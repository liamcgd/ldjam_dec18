﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BookPage : TextWrapper
{
    private HerbBookOpen book;
    [SerializeField] private string pageSide;
    [SerializeField] private TextMesh pageText;
    [SerializeField] private int lineLength = 60;

    public HerbBookOpen OpenBook
    {
        set
        {
            book = value;
        }
    }

    private void OnMouseDown()
    {
        book.TurnPage(pageSide);
    }

    public void AppendWrappedText(string s)
    {
        pageText.text += WrapText(lineLength, s);
    }

    public void AppendRawText(string s)
    {
        pageText.text += s;
    }

    public void ClearPage()
    {
        pageText.text = "";

        if (this.GetComponent<SpriteRendererFinder>() != null)
        {
            this.GetComponent<SpriteRendererFinder>().HerbSpriteRenderer.sprite = null;
        }
    }
}
