﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BookBackground : MonoBehaviour
{
    private HerbBookOpen book;

    public HerbBookOpen OpenBook
    {
        set
        {
            book = value;
        }
    }

    private void OnMouseDown()
    {
        book.CloseBook();
    }
}
