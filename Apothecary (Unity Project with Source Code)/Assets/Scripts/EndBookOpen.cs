﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndBookOpen : MonoBehaviour
{
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private AudioClip turnPageClip;
    [SerializeField] private AudioClip openCloseBookClip;
    private EndBookPage left;
    private EndBookPage right;
    private EndBookBackground area;
    private int currentPage = 0;
    private List<string> pages;

    public void InsertPages(EndBookPage l, EndBookPage r, EndBookBackground a, List<string> p)
    {
        left = l;
        right = r;
        area = a;
        pages = p;

        audioSource.clip = openCloseBookClip;
        audioSource.Play();

        TurnToPage(currentPage);
    }

    public void TurnPage(string pageSide)
    {
        bool turnPage = false;

        if (pageSide == "left")
        {
            if (currentPage > 1)
            {
                currentPage -= 2;
                turnPage = true;
            }
        }
        else if (pageSide == "right")
        {
            if (currentPage < pages.Count - 2)
            {
                currentPage += 2;
                turnPage = true;
            }
        }
        else
        {
            Debug.Log("Invalid page side. Must be left or right.");
            return;
        }

        if (turnPage)
        {
            audioSource.clip = turnPageClip;
            audioSource.Play();
            TurnToPage(currentPage);
        }
    }

    private void TurnToPage(int page)
    {
        left.ClearPage();
        right.ClearPage();

        if ((page >= 0) && (page <= pages.Count - 1))
        {
            left.AppendWrappedText(pages[page]);
        }

        if (((page + 1) >= 0) && ((page + 1) <= pages.Count - 1))
        {
            right.AppendWrappedText(pages[page + 1]);
        }
    }

    public void CloseBook()
    {
        GameObject.Find("Main Camera").GetComponent<GameController>().NextRound();

        Destroy(left.gameObject);
        Destroy(right.gameObject);
        Destroy(area.gameObject);
        Destroy(this.gameObject);
    }
}
