﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Herb
{
    private HerbID herbID;
    private string herbName;
    private int quantity;
    private string description;
    private Dictionary<SymptomID, bool> treatableSymptoms = new Dictionary<SymptomID, bool>();
    private int minHelpful;
    private int maxHelpful;
    private int minHarmful;
    private string negativeEffects;
    private int minLethal;

    public Herb (HerbID hID, string hName, string desc, List<SymptomID> cures, int minHelp, int maxHelp, int minHarm, string harm, int lethalDose)
    {
        herbID = hID;
        herbName = hName;
        quantity = 1;
        description = desc;

        foreach (SymptomID id in cures)
        {
            treatableSymptoms.Add(id, false);
        }

        minHelpful = minHelp;
        maxHelpful = maxHelp;
        minHarmful = minHarm;
        negativeEffects = harm;
        minLethal = lethalDose;
    }

    public HerbID HerbID
    {
        get
        {
            return herbID;
        }
    }

    public string Name
    {
        get
        {
            return herbName;
        }
    }

    public int Quantity
    {
        get
        {
            return quantity;
        }

        set
        {
            quantity = value;
        }
    }

    public string Description
    {
        get
        {
            return description;
        }
    }
    
    public int MinHelpful
    {
        get
        {
            return minHelpful;
        }
    }
    
    public int MaxHelpful
    {
        get
        {
            return maxHelpful;
        }
    }
    
    public int MinHarmful
    {
        get
        {
            return minHarmful;
        }
    }
    
    public string NegativeEffects
    {
        get
        {
            return negativeEffects;
        }
    }
    
    public int LethalDose
    {
        get
        {
            return minLethal;
        }
    }

    public Dictionary<SymptomID, bool> TreatableSymptoms
    {
        get
        {
            return treatableSymptoms;
        }
    }

    public bool DoseIsHelpful()
    {
        if ((quantity >= minHelpful) && (quantity <= maxHelpful))
        {
            return true;
        }

        return false;
    }

    public bool DoseIsHarmful()
    {
        if ((quantity >= minHarmful) && (quantity < minLethal))
        {
            return true;
        }

        return false;
    }

    public bool DoseIsLethal()
    {
        if (quantity >= minLethal)
        {
            return true;
        }

        return false;
    }
}