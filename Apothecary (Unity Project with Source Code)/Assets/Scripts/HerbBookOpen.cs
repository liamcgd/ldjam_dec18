﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HerbBookOpen : MonoBehaviour
{
    [SerializeField] private HerbBookClosed closedBook;
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private AudioClip turnPageClip;
    [SerializeField] private AudioClip openCloseBookClip;
    private BookPage left;
    private BookPage right;
    private BookBackground area;
    private int currentPage = 0;
    private Dictionary<HerbID, Herb> herbs = new Dictionary<HerbID, Herb>();
    private Dictionary<SymptomID, Symptom> symptoms = new Dictionary<SymptomID, Symptom>();

    public void InsertPages(BookPage l, BookPage r, BookBackground a)
    {
        left = l;
        right = r;
        area = a;
        herbs = GameObject.Find("Main Camera").GetComponent<Herbs>().ListOfHerbs;
        symptoms = GameObject.Find("Main Camera").GetComponent<Symptoms>().ListOfSymptoms;

        audioSource.clip = openCloseBookClip;
        audioSource.Play();

        TurnToPage(herbs[(HerbID)currentPage]);
    }

    public void TurnPage(string pageSide)
    {
        bool turnPage = false;

        if (pageSide == "left")
        {
            if (currentPage > 0)
            {
                currentPage -= 1;
                turnPage = true;
            }
        }
        else if (pageSide == "right")
        {
            if (currentPage < herbs.Count - 1)
            {
                currentPage += 1;
                turnPage = true;
            }
        }
        else
        {
            Debug.Log("Invalid page side. Must be left or right.");
            return;
        }

        if (turnPage)
        {
            audioSource.clip = turnPageClip;
            audioSource.Play();
            TurnToPage(herbs[(HerbID)currentPage]);
        }
    }

    private void TurnToPage(Herb herb)
    {
        bool foundUses = false;

        left.ClearPage();
        right.ClearPage();

        left.GetComponent<SpriteRendererFinder>().HerbSpriteRenderer.sprite = GameObject.Find("Main Camera").GetComponent<Herbs>().GetHerbSprite(herb.HerbID);

        left.AppendRawText("Name: " + herb.Name+ "\r\n");
        left.AppendWrappedText("\r\n" + "Description: " + herb.Description);

        right.AppendRawText("What it can help with:\r\n");

        foreach (KeyValuePair<SymptomID, Symptom> p in symptoms)
        {
            if (herb.TreatableSymptoms.ContainsKey(p.Key))
            { 
                if (herb.TreatableSymptoms[p.Key])
                {
                    right.AppendRawText("\r\n" + symptoms[p.Key].SymptomName);
                    foundUses = true;
                }
            }
        }

        if (!foundUses)
        {
            right.AppendRawText("\r\n\t\t" + "Unknown");
        }
    }

    public void CloseBook()
    {
        HerbBookClosed cb = Instantiate(closedBook);
        cb.Closed();

        Destroy(left.gameObject);
        Destroy(right.gameObject);
        Destroy(area.gameObject);
        Destroy(this.gameObject);
    }
}
