﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Symptoms : MonoBehaviour
{
    private Dictionary<SymptomID, Symptom> symptoms;

    public Dictionary<SymptomID, Symptom> ListOfSymptoms
    {
        get
        {
            return symptoms;
        }

        set
        {
            symptoms = value;
            //PrintSymptoms();
        }
    }

    /*private void PrintSymptoms()
    {
        Debug.Log("Printing details for " + symptoms.Count + " symptoms:");

        foreach (KeyValuePair<SymptomID, Symptom> p in symptoms)
        {
            Debug.Log("ID: " + p.Value.SymptomID);
            Debug.Log("Name: " + p.Value.SymptomName);
            // Debug.Log("Description: " + p.Value.SymptomDescription);
        }

        Debug.Log("Finished printing symptom details");
    }*/
}