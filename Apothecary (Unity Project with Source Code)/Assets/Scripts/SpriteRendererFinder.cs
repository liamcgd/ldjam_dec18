﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteRendererFinder : MonoBehaviour
{
    [SerializeField] private SpriteRenderer herbSpriteRenderer;

    public SpriteRenderer HerbSpriteRenderer
    {
        get
        {
            return herbSpriteRenderer;
        }
    }
}
