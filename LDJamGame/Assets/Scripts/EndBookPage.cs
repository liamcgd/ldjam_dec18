﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndBookPage : TextWrapper
{
    private EndBookOpen book;
    [SerializeField] private string pageSide;
    [SerializeField] private TextMesh pageText;
    [SerializeField] private int lineLength = 60;

    public EndBookOpen OpenBook
    {
        set
        {
            book = value;
        }
    }

    private void OnMouseDown()
    {
        book.TurnPage(pageSide);
    }

    public void AppendWrappedText(string s)
    {
        pageText.text += WrapText(lineLength, s);
    }

    public void AppendRawText(string s)
    {
        pageText.text += s;
    }

    public void ClearPage()
    {
        pageText.text = "";
    }
}
