﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Identifiable : MonoBehaviour
{
    private List<HerbID> identifiers = new List<HerbID>();

    public bool HasIdentifier(HerbID id)
    {
        bool result = false;

        if (identifiers.Contains(id))
        {
            result = true;
        }

        return result;
    }

    public void AddIdentifier(HerbID id)
    {
        if (!identifiers.Contains(id))
        {
            identifiers.Add(id);
        }
    }

    public void RemoveIdentifier(HerbID id)
    {
        if (identifiers.Contains(id))
        {
            identifiers.Remove(id);
        }
    }
}
