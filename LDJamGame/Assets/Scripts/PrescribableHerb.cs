﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrescribableHerb : MonoBehaviour
{
    [SerializeField] HerbID herbID;
    private Herb herb = null;

    private Herb Herb
    {
        get
        {
            if (herb == null)
            {
                herb = GameObject.Find("Main Camera").GetComponent<Herbs>().ListOfHerbs[herbID];
            }

            return herb;
        }
    }

    public void OnMouseDown()
    {
        PatientDoc[] docs = FindObjectsOfType<PatientDoc>();

        foreach (PatientDoc patDoc in docs)
        {
            if (patDoc.open)
            {
                patDoc.Patient.AddHerbToPrescription(Herb);
                patDoc.UpdateDisplayedPrescription();
            }
        }
    }
}
