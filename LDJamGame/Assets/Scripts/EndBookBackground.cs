﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndBookBackground : MonoBehaviour
{
    private EndBookOpen book;

    public EndBookOpen OpenBook
    {
        set
        {
            book = value;
        }
    }

    private void OnMouseDown()
    {
        book.CloseBook();
    }
}
