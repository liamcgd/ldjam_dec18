﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HerbBookClosed : MonoBehaviour
{
    [SerializeField] private GameObject openBook;
    [SerializeField] private GameObject bookLeftPage;
    [SerializeField] private GameObject bookRightPage;
    [SerializeField] private GameObject bookBackgroundBarrier;
    [SerializeField] private DoubleClickable cover;
    [SerializeField] private AudioSource audioSource;

    public void Closed()
    {
        audioSource.Play();
    }

    private void OnMouseDown()
    {
        OpenBook();
    }

    private void OpenBook()
    {
        GameObject ob = Instantiate(openBook);
        GameObject lp = Instantiate(bookLeftPage);
        GameObject rp = Instantiate(bookRightPage);
        GameObject bbb = Instantiate(bookBackgroundBarrier);

        ob.GetComponent<HerbBookOpen>().InsertPages(lp.GetComponent<BookPage>(), rp.GetComponent<BookPage>(), bbb.GetComponent<BookBackground>());
        lp.GetComponent<BookPage>().OpenBook = ob.GetComponent<HerbBookOpen>();
        rp.GetComponent<BookPage>().OpenBook = ob.GetComponent<HerbBookOpen>();
        bbb.GetComponent<BookBackground>().OpenBook = ob.GetComponent<HerbBookOpen>();

        Destroy(this.gameObject);
    }
}
