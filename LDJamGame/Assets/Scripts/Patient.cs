﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patient
{
    private string patientName;
    private int patientAge;
    private string description;
    private string symptomsDescription;
    private List<Symptom> untreatedSymptoms;
    private List<Symptom> treatedSymptoms = new List<Symptom>();
    private Dictionary<HerbID, Herb> prescribedHerbs = new Dictionary<HerbID, Herb>();

    public Patient(string name, int age, string desc, string symptoms, List<Symptom> symptomList)
    {
        patientName = name;
        patientAge = age;
        description = desc;
        symptomsDescription = symptoms;
        untreatedSymptoms = symptomList;
    }

    public string Name
    {
        get
        {
            return patientName;
        }
    }

    public int Age
    {
        get
        {
            return patientAge;
        }
    }

    public string Description
    {
        get
        {
            return description;
        }
    }

    public string SymptomsDescription
    {
        get
        {
            return symptomsDescription;
        }
    }

    public List<Symptom> UntreatedSymptoms
    {
        get
        {
            return untreatedSymptoms;
        }
    }

    public Dictionary<HerbID, Herb> PrescribedHerbs
    {
        get
        {
            return prescribedHerbs;
        }
    }

    public void AddHerbToPrescription(Herb herb)
    {
        if (prescribedHerbs.ContainsKey(herb.HerbID))
        {
            prescribedHerbs[herb.HerbID].Quantity += 1;
        }
        else
        {
            prescribedHerbs.Add(herb.HerbID, CopyHerbValues(herb));
        }
    }

    // Duplicates all the values of herb and assigns them to a new herb object to be assigned to prescribedHerbs rather than the original
    // so as to keep the original and the data structures (dictionary, list, etc.) it contains separate from the originals when the copy
    // gets assigned to prescribedHerbs. Mainly just to keep the quantity of the copy distinct from the quantity of the original. May 
    // be overkill, but meh. 
    private Herb CopyHerbValues(Herb h)
    {
        List<SymptomID> s = new List<SymptomID>();

        foreach (KeyValuePair<SymptomID, bool> p in h.TreatableSymptoms)
        {
            s.Add(p.Key);
        }

        return new Herb(h.HerbID, h.Name, h.Description, s, h.MinHelpful, h.MaxHelpful, h.MinHarmful, h.NegativeEffects, h.LethalDose);
    }

    public string TreatPatient()
    {
        string result = "";
        List<HerbID> usedHerbs = new List<HerbID>();

        foreach (KeyValuePair<HerbID, Herb> p in prescribedHerbs)
        {
            if (p.Value.DoseIsHelpful())
            {
                TreatWithHerb(p.Value);
                usedHerbs.Add(p.Key);
            }
        }

        foreach (HerbID id in usedHerbs)
        {
            prescribedHerbs.Remove(id);
        }

        if (untreatedSymptoms.Count == 0)
        {
            return "You have properly treated " + Name + ".";
        }
        else
        {
            //Print list of symptoms that were cured
            result += "You have properly treated the following of " + Name + "'s symptoms:";

            if (treatedSymptoms.Count == 0)
            {
                result += "\r\n\tNone";
            }
            else
            {
                foreach (Symptom s in treatedSymptoms)
                {
                    result += "\r\n\t" + s.SymptomName;
                }
            }

            result += CheckUntreatedSymptoms();

            return result;
        }
    }

    private void TreatWithHerb(Herb herb)
    {
        foreach (Symptom s in untreatedSymptoms)
        {
            if (herb.TreatableSymptoms.ContainsKey(s.SymptomID))
            {
                treatedSymptoms.Add(s);
                //untreatedSymptoms.Remove(s);
                GameObject.Find("Main Camera").GetComponent<Herbs>().ListOfHerbs[herb.HerbID].TreatableSymptoms[s.SymptomID] = true;
            }
        }

        foreach (Symptom s in treatedSymptoms)
        {
            if (untreatedSymptoms.Contains(s))
            {
                untreatedSymptoms.Remove(s);
            }
        }
    }

    private string CheckUntreatedSymptoms()
    {
        string result = "\r\nYou didn't cure the following of " + Name + "'s symptoms:";

        foreach (Symptom s in untreatedSymptoms)
        {
            result += "\r\n\t" + s.SymptomName;
        }

        if (prescribedHerbs.Count > 0)
        {
            result += CheckSideEffects();
        }

        return result;
    }

    private string CheckSideEffects()
    {
        List<string> sideEffects = new List<string>();
        string result = "";

        foreach (KeyValuePair<HerbID, Herb> p in prescribedHerbs)
        {
            if (p.Value.DoseIsHarmful())
            {
                sideEffects.Add(p.Value.NegativeEffects);
                prescribedHerbs.Remove(p.Key);
            }
        }

        if (sideEffects.Count > 0)
        {
            result += "\r\nYou also gave them these side effects:";

            foreach (string s in sideEffects)
            {
                result += "\r\n\t" + s; //print side effect
                //make sure to wrap text for side effects
            }
        }

        if (prescribedHerbs.Count > 0)
        {
            result += CheckLethality();
        }

        return result;
    }

    private string CheckLethality()
    {
        foreach (KeyValuePair<HerbID, Herb> p in prescribedHerbs)
        {
            if (p.Value.DoseIsLethal())
            {
                return "\r\n You also administered a lethal dose of " + p.Value.Name + "."; //Print message saying you administered a lethal does of h
            }
        }

        return "";
    }
}
